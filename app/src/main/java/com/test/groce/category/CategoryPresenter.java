package com.test.groce.category;

import android.content.Context;

import com.test.groce.category.model.CategoryModel;

import java.util.ArrayList;

public class CategoryPresenter implements CategoryCallbacklistner.CategoryManagerCallback {
    private Context context;
    private CategoryCallbacklistner.CategoryView categoryView;
    private CategoryManager categoryManager;

    public CategoryPresenter(Context context, CategoryCallbacklistner.CategoryView categoryView) {
        this.context = context;
        this.categoryView = categoryView;
        this.categoryManager = new CategoryManager(context,this);
    }

    public void insertCategory(CategoryModel categoryModel){
        categoryManager.insertCategory(categoryModel);
    }

    public void deleteCategory(String categoryId){
        categoryManager.deleteCategory(categoryId);
    }

    public void getAllCategoryList(){
        categoryManager.getAllCategoryList();
    }

    @Override
    public void onSuccessCategoryAdded(String successMessage) {
         categoryView.onSuccessCategoryAdded(successMessage);
    }

    @Override
    public void onSuccessDeleteCategory(String successMessage) {
        categoryView.onSuccessDeleteCategory(successMessage);
    }

    @Override
    public void onSuccessCategoryList(ArrayList<CategoryModel> categoryModelArrayList) {
        categoryView.onSuccessCategoryList(categoryModelArrayList);
    }

    @Override
    public void onError(String errorMessage) {
        categoryView.onError(errorMessage);
    }
}
