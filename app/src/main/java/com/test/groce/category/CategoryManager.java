package com.test.groce.category;

import android.content.Context;

import com.test.groce.category.model.CategoryModel;
import com.test.groce.databasehelper.DBHelperSingleton;

import java.util.ArrayList;

public class CategoryManager {
    private Context context;
    private CategoryCallbacklistner.CategoryManagerCallback categoryManagerCallback;

    public CategoryManager(Context context, CategoryCallbacklistner.CategoryManagerCallback categoryManagerCallback) {
        this.context = context;
        this.categoryManagerCallback = categoryManagerCallback;
    }

    public void insertCategory(CategoryModel categoryModel){
        boolean isCategoryInserted = DBHelperSingleton.getInstance().mDBDbHelper.insertCategoryFromLocal(categoryModel);
        if(isCategoryInserted){
            categoryManagerCallback.onSuccessCategoryAdded("Category inserted successfully");
        }else {
            categoryManagerCallback.onError("Category inserted failed!");
        }
    }

    public void deleteCategory(String categoryId){
        boolean isCategoryDelete = DBHelperSingleton.getInstance().mDBDbHelper.deleteCategoryFromLocal(categoryId);
        if(isCategoryDelete){
            categoryManagerCallback.onSuccessDeleteCategory("Category Deleted with all items successfully");
        }else {
            categoryManagerCallback.onError("Category Delete failed!");
        }
    }

    public void getAllCategoryList(){
        ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();
        categoryModelArrayList.addAll(DBHelperSingleton.getInstance().mDBDbHelper.getAllCategoryFromLocal());
        if(categoryModelArrayList.isEmpty()){
            categoryManagerCallback.onError("No Data Found!");
        }else {
            categoryManagerCallback.onSuccessCategoryList(categoryModelArrayList);
        }
    }
}
