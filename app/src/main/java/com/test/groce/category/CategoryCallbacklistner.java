package com.test.groce.category;

import com.test.groce.basic.BaseInterface;
import com.test.groce.category.model.CategoryModel;

import java.util.ArrayList;

public class CategoryCallbacklistner {
    public interface CategoryView extends BaseInterface {
        void onSuccessCategoryAdded(String successMessage);
        void onSuccessDeleteCategory(String successMessage);
        void onSuccessCategoryList(ArrayList<CategoryModel>categoryModelArrayList);
    }

    public interface CategoryManagerCallback {
        void onSuccessCategoryAdded(String successMessage);
        void onSuccessDeleteCategory(String successMessage);
        void onSuccessCategoryList(ArrayList<CategoryModel>categoryModelArrayList);
        void onError(String errorMessage);
    }
}
