package com.test.groce.category.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.test.groce.R;
import com.test.groce.category.model.CategoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    @BindView(R.id.textViewCategoryName)
    TextView textViewCategoryName;
    @BindView(R.id.imageViewDelete)
    ImageView imageViewDelete;
    @BindView(R.id.linearlayoutClick)
    LinearLayout linearlayoutClick;
    private Context context;
    private ArrayList<CategoryModel> categoryModels;
    private CategoryItemCallbackListner categoryItemCallbackListner;


    public CategoryAdapter(Context context, ArrayList<CategoryModel> categoryModels, CategoryItemCallbackListner categoryItemCallbackListner) {
        this.context = context;
        this.categoryModels = categoryModels;
        this.categoryItemCallbackListner = categoryItemCallbackListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_category_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCategoryName.setText(categoryModels.get(position).getCategoryName());
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCategoryName)
        TextView textViewCategoryName;
        @BindView(R.id.linearlayoutClick)
        LinearLayout linearlayoutClick;

        @OnClick({R.id.imageViewDelete, R.id.linearlayoutClick})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewDelete:
                    categoryItemCallbackListner.CategoryDeleteClick(getAdapterPosition());
                    break;
                case R.id.linearlayoutClick:
                    categoryItemCallbackListner.CategoryItemClick(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CategoryItemCallbackListner {
        void CategoryItemClick(int position);
        void CategoryDeleteClick(int position);
    }
}
