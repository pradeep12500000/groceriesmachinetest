package com.test.groce.category;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.test.groce.R;
import com.test.groce.basic.BaseActivity;
import com.test.groce.category.adapter.CategoryAdapter;
import com.test.groce.category.model.CategoryModel;
import com.test.groce.sub_category.SubCategoryActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends BaseActivity implements CategoryAdapter.CategoryItemCallbackListner, CategoryCallbacklistner.CategoryView {
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewHeaderName)
    TextView textViewHeaderName;
    @BindView(R.id.imageViewAddCategory)
    ImageView imageViewAddCategory;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.recyclerViewCategory)
    RecyclerView recyclerViewCategory;
    @BindView(R.id.imageViewNoRecord)
    ImageView imageViewNoRecord;
    @BindView(R.id.relativeLayoutNoRecordFound)
    RelativeLayout relativeLayoutNoRecordFound;
    private Context context = this;
    private ArrayList<CategoryModel> CategoryModels;
    private CategoryPresenter categoryPresenter;
    private EditText editTextCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        categoryPresenter = new CategoryPresenter(context, this);
        categoryPresenter.getAllCategoryList();
    }

    private void setDataAdapter() {
        CategoryAdapter subCategoryAdapter = new CategoryAdapter(context, CategoryModels, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewCategory) {
            recyclerViewCategory.setAdapter(subCategoryAdapter);
            recyclerViewCategory.setLayoutManager(layoutManager);
        }
    }

    @OnClick({R.id.imageViewBack, R.id.imageViewAddCategory})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                onBackPressed();
                break;
            case R.id.imageViewAddCategory:
                AddDialog();
                break;
        }
    }


    private void AddDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_add_category);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        editTextCategory = dialog.findViewById(R.id.editTextCategory);

        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    callCategoryAdd(editTextCategory.getText().toString());
                    dialog.cancel();
                }
            }
        });
    }


    private boolean validate() {
        if (editTextCategory.getText().toString().trim().length() == 0) {
            showToast("Enter Category !");
            return false;
        }
        return true;
    }

    private void callCategoryAdd(String CategoryName) {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setCategoryName(CategoryName);
        categoryPresenter.insertCategory(categoryModel);
    }

    @Override
    public void CategoryItemClick(int position) {
        Intent intent = new Intent(context, SubCategoryActivity.class);
        intent.putExtra("CategoryId", CategoryModels.get(position).getCategoryId());
        startActivity(intent);
    }

    @Override
    public void CategoryDeleteClick(int position) {
        categoryPresenter.deleteCategory(CategoryModels.get(position).getCategoryId() + "");
    }

    @Override
    public void onSuccessCategoryAdded(String successMessage) {
        showToast(successMessage);
        categoryPresenter.getAllCategoryList();
    }

    @Override
    public void onSuccessDeleteCategory(String successMessage) {
        showToast(successMessage);
        categoryPresenter.getAllCategoryList();
    }

    @Override
    public void onSuccessCategoryList(ArrayList<CategoryModel> categoryModelArrayList) {
        CategoryModels = new ArrayList<>();
        CategoryModels.addAll(categoryModelArrayList);
        setDataAdapter();
        relativeLayoutNoRecordFound.setVisibility(View.GONE);
        recyclerViewCategory.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
        if(errorMessage.equalsIgnoreCase("No Data Found!")){
            relativeLayoutNoRecordFound.setVisibility(View.VISIBLE);
            recyclerViewCategory.setVisibility(View.GONE);
        }
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }
}
