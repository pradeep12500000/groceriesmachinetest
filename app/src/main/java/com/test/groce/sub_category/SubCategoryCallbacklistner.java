package com.test.groce.sub_category;

import com.test.groce.basic.BaseInterface;
import com.test.groce.sub_category.model.SubCategoryModel;

import java.util.ArrayList;

public class SubCategoryCallbacklistner {
    public interface SubCategoryView extends BaseInterface {
        void onSuccessItemsAdd(String successMessage);
        void onSuccessDeleteItems(String successMessage);
        void onSuccessItemsList(ArrayList<SubCategoryModel> categoryModelArrayList);
        void onSuccessUpdateItems(String successMessage);
    }

    public interface SubCategoryManagerCallback{
        void onSuccessItemsAdd(String successMessage);
        void onSuccessDeleteItems(String successMessage);
        void onSuccessItemsList(ArrayList<SubCategoryModel> categoryModelArrayList);
        void onSuccessUpdateItems(String successMessage);
        void onError(String errorMessage);
    }
}
