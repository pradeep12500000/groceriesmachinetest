package com.test.groce.sub_category.model;

public class SubCategoryModel {
    int CategoryId,ProductId,isDisplay;
    String ProductName;

    public SubCategoryModel() {
    }

    public SubCategoryModel(int categoryId, int productId,int isDisplay, String productName) {
        CategoryId = categoryId;
        ProductId = productId;
        this.isDisplay = isDisplay;
        ProductName = productName;
    }

    public int getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(int isDisplay) {
        this.isDisplay = isDisplay;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }
}
