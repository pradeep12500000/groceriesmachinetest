package com.test.groce.sub_category;

import android.content.Context;

import com.test.groce.sub_category.model.SubCategoryModel;

import java.util.ArrayList;

public class SubCategoryPresenter implements SubCategoryCallbacklistner.SubCategoryManagerCallback {
    private Context context;
    private  SubCategoryManager subCategoryManager;
    private SubCategoryCallbacklistner.SubCategoryView subCategoryView;

    public SubCategoryPresenter(Context context, SubCategoryCallbacklistner.SubCategoryView subCategoryView) {
        this.context = context;
        this.subCategoryView = subCategoryView;
        this.subCategoryManager = new SubCategoryManager(context,this);
    }

    public void updateItems(SubCategoryModel subCategoryModel){
         subCategoryManager.updateItems(subCategoryModel);
    }

    public void insertItems(SubCategoryModel subCategoryModel){
        subCategoryManager.insertItems(subCategoryModel);
    }

    public void deleteItems(String itemId){
        subCategoryManager.deleteItems(itemId);
    }

    public void getAllItemsList(String categoryId){
        subCategoryManager.getAllItemsList(categoryId);
    }

    @Override
    public void onSuccessItemsAdd(String successMessage) {
        subCategoryView.onSuccessItemsAdd(successMessage);
    }

    @Override
    public void onSuccessDeleteItems(String successMessage) {
        subCategoryView.onSuccessDeleteItems(successMessage);
    }

    @Override
    public void onSuccessItemsList(ArrayList<SubCategoryModel> categoryModelArrayList) {
        subCategoryView.onSuccessItemsList(categoryModelArrayList);
    }

    @Override
    public void onSuccessUpdateItems(String successMessage) {
      subCategoryView.onSuccessUpdateItems(successMessage);
    }

    @Override
    public void onError(String errorMessage) {
        subCategoryView.onError(errorMessage);
    }
}
