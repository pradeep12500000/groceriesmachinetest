package com.test.groce.sub_category;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.test.groce.R;
import com.test.groce.basic.BaseActivity;
import com.test.groce.sub_category.adapter.SubCategoryAdapter;
import com.test.groce.sub_category.model.SubCategoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubCategoryActivity extends BaseActivity implements SubCategoryAdapter.SubCategoryItemCallbackListner, SubCategoryCallbacklistner.SubCategoryView {
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewHeaderName)
    TextView textViewHeaderName;
    @BindView(R.id.imageViewAddProduct)
    ImageView imageViewAddProduct;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.recyclerViewSubCategory)
    RecyclerView recyclerViewSubCategory;
    @BindView(R.id.imageViewNoRecord)
    ImageView imageViewNoRecord;
    @BindView(R.id.relativeLayoutNoRecordFound)
    RelativeLayout relativeLayoutNoRecordFound;
    private Context context = this;
    private ArrayList<SubCategoryModel> subCategoryModels;
    private SubCategoryPresenter subCategoryPresenter;
    private Integer CategoryId;
    private EditText editTextProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        ButterKnife.bind(this);
        subCategoryPresenter = new SubCategoryPresenter(context, this);
        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        if (b != null) {
            CategoryId = (Integer) b.get("CategoryId");
        }
        subCategoryPresenter.getAllItemsList(CategoryId + "");
    }

    private void setDataAdapter() {
        SubCategoryAdapter subCategoryAdapter = new SubCategoryAdapter(context, subCategoryModels, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewSubCategory) {
            recyclerViewSubCategory.setAdapter(subCategoryAdapter);
            recyclerViewSubCategory.setLayoutManager(layoutManager);
        }
    }

    @OnClick({R.id.imageViewBack, R.id.imageViewAddProduct})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                onBackPressed();
                break;
            case R.id.imageViewAddProduct:
                AddDialog();
                break;
        }
    }

    private void AddDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_add_product);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        editTextProduct = dialog.findViewById(R.id.editTextProduct);

        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    callAddProduct(editTextProduct.getText().toString());
                    dialog.cancel();
                }
            }
        });
    }

    private boolean validate() {
        if (editTextProduct.getText().toString().trim().length() == 0) {
            showToast("Enter Product !");
            return false;
        }
        return true;
    }

    private void callAddProduct(String ProductName) {
        SubCategoryModel subCategoryModel = new SubCategoryModel();
        subCategoryModel.setProductId(0);
        subCategoryModel.setCategoryId(CategoryId);
        subCategoryModel.setProductName(ProductName);
        subCategoryPresenter.insertItems(subCategoryModel);
    }

    @Override
    public void onSubCategoryCheckedClicked(int position) {
        SubCategoryModel subCategoryModel = new SubCategoryModel();
        subCategoryModel.setProductId(subCategoryModels.get(position).getProductId());
        if (subCategoryModels.get(position).getIsDisplay() == 1) {
            subCategoryModel.setIsDisplay(0);
        } else {
            subCategoryModel.setIsDisplay(1);
        }
        subCategoryPresenter.updateItems(subCategoryModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        subCategoryPresenter.getAllItemsList(CategoryId + "");
    }

    @Override
    public void onSubCategoryDeleteClicked(int position) {
        subCategoryPresenter.deleteItems(subCategoryModels.get(position).getProductId() + "");
    }

    @Override
    public void onSuccessItemsAdd(String successMessage) {
        showToast(successMessage);
        subCategoryPresenter.getAllItemsList(CategoryId + "");
    }

    @Override
    public void onSuccessDeleteItems(String successMessage) {
        showToast(successMessage);
        subCategoryPresenter.getAllItemsList(CategoryId + "");
    }

    @Override
    public void onSuccessItemsList(ArrayList<SubCategoryModel> categoryModelArrayList) {
        subCategoryModels = new ArrayList<>();
        subCategoryModels.addAll(categoryModelArrayList);
        setDataAdapter();
        relativeLayoutNoRecordFound.setVisibility(View.GONE);
        recyclerViewSubCategory.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccessUpdateItems(String successMessage) {
        showToast(successMessage);
        subCategoryPresenter.getAllItemsList(CategoryId + "");

    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
        if (errorMessage.equalsIgnoreCase("No Data Found!")) {
            relativeLayoutNoRecordFound.setVisibility(View.VISIBLE);
            recyclerViewSubCategory.setVisibility(View.GONE);
        }
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }
}
