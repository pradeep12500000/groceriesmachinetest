package com.test.groce.sub_category;

import android.content.Context;

import com.test.groce.databasehelper.DBHelperSingleton;
import com.test.groce.sub_category.model.SubCategoryModel;

import java.util.ArrayList;

public class SubCategoryManager {
    private Context context;
    private SubCategoryCallbacklistner.SubCategoryManagerCallback subCategoryManagerCallback;

    public SubCategoryManager(Context context, SubCategoryCallbacklistner.SubCategoryManagerCallback subCategoryManagerCallback) {
        this.context = context;
        this.subCategoryManagerCallback = subCategoryManagerCallback;
    }

    public void updateItems(SubCategoryModel subCategoryModel){
        boolean isUpdateSuccessfully = DBHelperSingleton.getInstance().mDBDbHelper.updateItemsFromLocal(subCategoryModel);
        if(isUpdateSuccessfully){
            subCategoryManagerCallback.onSuccessUpdateItems("Items updated successfully");
        }else {
            subCategoryManagerCallback.onError("Updated failed!");
        }
    }

    public void insertItems(SubCategoryModel subCategoryModel){
        boolean isItemsInserted = DBHelperSingleton.getInstance().mDBDbHelper.insertItemsFromLocal(subCategoryModel);
        if(isItemsInserted){
            subCategoryManagerCallback.onSuccessItemsAdd("Items inserted successfully");
        }else {
            subCategoryManagerCallback.onError("Items inserted failed!");
        }
    }

    public void deleteItems(String itemId){
        boolean isItemsDelete = DBHelperSingleton.getInstance().mDBDbHelper.deleteItemsFromLocal(itemId);
        if(isItemsDelete){
            subCategoryManagerCallback.onSuccessDeleteItems("Items Deleted successfully");
        }else {
            subCategoryManagerCallback.onError("Items Delete failed!");
        }
    }

    public void getAllItemsList(String categoryId){
        ArrayList<SubCategoryModel> subCategoryModelArrayList = new ArrayList<>();
        subCategoryModelArrayList.addAll(DBHelperSingleton.getInstance().mDBDbHelper.getAllItemsFromLocal(categoryId));
        if(subCategoryModelArrayList.isEmpty()){
            subCategoryManagerCallback.onError("No Data Found!");
        }else {
            subCategoryManagerCallback.onSuccessItemsList(subCategoryModelArrayList);
        }
    }
}
