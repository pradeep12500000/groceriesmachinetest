package com.test.groce.sub_category.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.test.groce.R;
import com.test.groce.sub_category.model.SubCategoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {

    @BindView(R.id.textViewProductName)
    TextView textViewProductName;
    @BindView(R.id.checkboxItemCheck)
    CheckBox checkboxItemCheck;
    @BindView(R.id.imageViewDelete)
    ImageView imageViewDelete;
    private Context context;
    private ArrayList<SubCategoryModel> subCategoryModels;
    private SubCategoryItemCallbackListner subCategoryItemCallbackListner;

    public SubCategoryAdapter(Context context, ArrayList<SubCategoryModel> subCategoryModels, SubCategoryItemCallbackListner subCategoryItemCallbackListner) {
        this.context = context;
        this.subCategoryModels = subCategoryModels;
        this.subCategoryItemCallbackListner = subCategoryItemCallbackListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_sub_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewProductName.setText(subCategoryModels.get(position).getProductName());
        if (subCategoryModels.get(position).getIsDisplay() == 1) {
            holder.checkboxItemCheck.setChecked(true);
        } else {
            holder.checkboxItemCheck.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return subCategoryModels.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        @BindView(R.id.checkboxItemCheck)
        CheckBox checkboxItemCheck;

        @OnClick({R.id.checkboxItemCheck, R.id.imageViewDelete})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.checkboxItemCheck:
                    subCategoryItemCallbackListner.onSubCategoryCheckedClicked(getAdapterPosition());
                    break;
                case R.id.imageViewDelete:
                    subCategoryItemCallbackListner.onSubCategoryDeleteClicked(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SubCategoryItemCallbackListner {
        void onSubCategoryCheckedClicked(int position);
        void onSubCategoryDeleteClicked(int position);
    }
}
