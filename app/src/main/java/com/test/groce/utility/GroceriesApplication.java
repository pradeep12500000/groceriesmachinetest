package com.test.groce.utility;

import android.app.Application;

import com.test.groce.databasehelper.DBHelperSingleton;

public class GroceriesApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DBHelperSingleton.getInstance().initDB(this);
    }
}
