package com.test.groce.utility;


public class Constants {

    /*constants for databse*/
    public static final String CATEGORY_TABLE="categoryTable";

    public static final String CATEGORY_ID = "catId";
    public static final String CATEGORY_NAME = "catName";

    public static final String CREATE_TABLE_CATEGORY="CREATE TABLE IF  NOT EXISTS " + CATEGORY_TABLE + "("
            + CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + CATEGORY_NAME + " TEXT" + ")";

    public static final String DROP_TABLE_CATEGORY="DROP TABLE IF EXISTS"+CATEGORY_TABLE;
    public static final String SELECT_TABLE_CATEGORY="SELECT * FROM "+ CATEGORY_TABLE +" ORDER BY "+CATEGORY_NAME+" ASC";



    /*recent table constant*/
    public static final String ITEMS_TABLE="itemsTable";

    public static final String ITEM_ID = "itemID";
    public static final String ITEM_NAME = "itemName";
    public static final String IS_DISPLAY = "isDisplay";
    public static final String CAT_ID_ITEM = "categoryID";


    public static final String CREATE_TABLE_ITEM = "CREATE TABLE IF NOT EXISTS "+
            ITEMS_TABLE + "("+ ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ITEM_NAME + " TEXT,"
            + IS_DISPLAY + " INTEGER,"
            + CAT_ID_ITEM + " INTEGER"+ ")";

    public static final String DROP_TABLE_ITEM="DROP TABLE IF EXISTS"+ITEMS_TABLE;

    public static final String SELECT_TABLE_ITEM="SELECT * FROM "+ ITEMS_TABLE+" ORDER BY "+ITEM_NAME+" ASC";






}
