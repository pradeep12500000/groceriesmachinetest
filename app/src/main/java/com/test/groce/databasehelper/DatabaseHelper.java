package com.test.groce.databasehelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.test.groce.category.model.CategoryModel;
import com.test.groce.home.model.HomeModel;
import com.test.groce.sub_category.model.SubCategoryModel;
import com.test.groce.utility.Constants;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final int DB_VERSION = 1;
    public static final String DATABASE_NAME = "groceries.db";
    private DBInteraction dbInteraction;

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME,null, DB_VERSION);
        this.dbInteraction = new DBInteraction(this);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constants.CREATE_TABLE_CATEGORY);
        db.execSQL(Constants.CREATE_TABLE_ITEM);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Constants.DROP_TABLE_CATEGORY);
        db.execSQL(Constants.DROP_TABLE_ITEM);
    }


    /*----------------------------------------function start from here -------------------------------------------*/

    public boolean insertCategoryFromLocal(CategoryModel categoryModel){
        return dbInteraction.insertCategory(categoryModel);
    }

    public boolean insertItemsFromLocal(SubCategoryModel subCategoryModel) {
        return dbInteraction.insertItems(subCategoryModel);
    }

    public boolean updateItemsFromLocal(SubCategoryModel subCategoryModel){
          return dbInteraction.updateItems(subCategoryModel);
    }

    public boolean deleteCategoryFromLocal(String categoryId){
        return dbInteraction.deleteCategory(categoryId);
    }

    // in delete items arraylist require because multiple items may be deleted at a time
    public boolean deleteItemsFromLocal(String itemsId){
        return dbInteraction.deleteItems(itemsId);
    }



    public ArrayList<CategoryModel> getAllCategoryFromLocal() {
        return dbInteraction.getAllCategory();
    }

    public ArrayList<HomeModel> getHomeArrayListFromLocal() {
        return dbInteraction.getHomeArrayList();
    }

    public ArrayList<SubCategoryModel> getAllItemsFromLocal(String categoryId) {
        return dbInteraction.getAllItems(categoryId);
    }


}
