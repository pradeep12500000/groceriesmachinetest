package com.test.groce.databasehelper;

import android.content.Context;

public class DBHelperSingleton {
    public static DatabaseHelper mDBDbHelper = null;
    private static final DBHelperSingleton ourInstance = new DBHelperSingleton();

    public static DBHelperSingleton getInstance() {
        return ourInstance;
    }

    private DBHelperSingleton() {
    }

    public void initDB(Context context){
        mDBDbHelper = new DatabaseHelper(context);
    }
}
