package com.test.groce.databasehelper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.test.groce.category.model.CategoryModel;
import com.test.groce.home.model.HomeModel;
import com.test.groce.home.model.HomeSubmodel;
import com.test.groce.sub_category.model.SubCategoryModel;
import com.test.groce.utility.Constants;

import java.util.ArrayList;

public class DBInteraction {

    private SQLiteDatabase dbReadable, dbWritable;

    public DBInteraction(DatabaseHelper databaseHandler) {
        this.dbReadable = databaseHandler.getReadableDatabase();
        this.dbWritable = databaseHandler.getWritableDatabase();
    }

    public void deleteAllDataFromTable() {
        dbWritable.execSQL("DELETE FROM " + Constants.CATEGORY_TABLE);
        dbWritable.execSQL("DELETE FROM " + Constants.ITEMS_TABLE);
    }

    /* cateogry start*/

    public boolean insertCategory(CategoryModel categoryModel) {
        long insertedID = dbWritable.insert(Constants.CATEGORY_TABLE, null, this.getCategoryObject(categoryModel));
        if (insertedID != -1) {
            return true;
        } else {
            return false;
        }
    }

    private ContentValues getCategoryObject(CategoryModel categoryModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.CATEGORY_NAME, categoryModel.getCategoryName());
        return contentValues;
    }

    public boolean deleteCategory(String categoryId) {
        //delete first all items with respect to category
        Cursor cursor = dbReadable.rawQuery("SELECT * FROM " + Constants.ITEMS_TABLE + " WHERE "
                + Constants.CAT_ID_ITEM + " = '" + Integer.parseInt(categoryId) + "' COLLATE NOCASE ", null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    int itemsId = cursor.getInt(cursor.getColumnIndex(Constants.ITEM_ID));
                    dbWritable.delete(Constants.ITEMS_TABLE, "itemID = ? ", new String[]{itemsId + ""});

                } while (cursor.moveToNext());
            }
        }

        //now delete category
        return dbWritable.delete(Constants.CATEGORY_TABLE, "catId = ? ", new String[]{categoryId}) > 0;
    }

    public ArrayList<CategoryModel> getAllCategory() {
        ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constants.SELECT_TABLE_CATEGORY, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    CategoryModel datum = new CategoryModel();
                    datum.setCategoryId(cursor.getInt(cursor.getColumnIndex(Constants.CATEGORY_ID)));
                    datum.setCategoryName(cursor.getString(cursor.getColumnIndex(Constants.CATEGORY_NAME)));
                    categoryModelArrayList.add(datum);

                } while (cursor.moveToNext());
            }
            return categoryModelArrayList;
        }
        return categoryModelArrayList;
    }


    public ArrayList<HomeModel> getHomeArrayList() {
        ArrayList<HomeModel> homeModelArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constants.SELECT_TABLE_CATEGORY, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    HomeModel datum = new HomeModel();
                    datum.setCategoryId(cursor.getInt(cursor.getColumnIndex(Constants.CATEGORY_ID)));
                    datum.setCategoryName(cursor.getString(cursor.getColumnIndex(Constants.CATEGORY_NAME)));
                    datum.setHomeSubmodel(getArrayItemList(datum.getCategoryId()));
                    homeModelArrayList.add(datum);

                } while (cursor.moveToNext());
            }
            return homeModelArrayList;
        }
        return homeModelArrayList;
    }


    private ArrayList<HomeSubmodel> getArrayItemList(int categoryId) {
        ArrayList<HomeSubmodel> homeSubmodelArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery("SELECT * FROM " + Constants.ITEMS_TABLE + " WHERE "
                + Constants.CAT_ID_ITEM + " = '" + Integer.parseInt(categoryId + "") + "' AND isDisplay = '1' COLLATE NOCASE ", null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    HomeSubmodel homeSubmodel = new HomeSubmodel();
                    homeSubmodel.setCategoryId(cursor.getInt(cursor.getColumnIndex(Constants.CAT_ID_ITEM)));
                    homeSubmodel.setProductId(cursor.getInt(cursor.getColumnIndex(Constants.ITEM_ID)));
                    homeSubmodel.setProductName(cursor.getString(cursor.getColumnIndex(Constants.ITEM_NAME)));
                    homeSubmodel.setIsDisplay(cursor.getInt(cursor.getColumnIndex(Constants.IS_DISPLAY)));
                    homeSubmodelArrayList.add(homeSubmodel);

                } while (cursor.moveToNext());
            }
            return homeSubmodelArrayList;
        }
        return homeSubmodelArrayList;
    }


    /*cateogry Finish*/


    /* items start*/
    public boolean insertItems(SubCategoryModel subCategoryModel) {
        long insertedID = dbWritable.insert(Constants.ITEMS_TABLE, null, this.getItemsObject(subCategoryModel));
        if (insertedID != -1) {
            return true;
        } else {
            return false;
        }
    }

    private ContentValues getItemsObject(SubCategoryModel subCategoryModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.ITEM_NAME, subCategoryModel.getProductName());
        contentValues.put(Constants.CAT_ID_ITEM, subCategoryModel.getCategoryId());
        contentValues.put(Constants.IS_DISPLAY, 0);
        return contentValues;
    }


    public boolean updateItems(SubCategoryModel subCategoryModel) {
        long updateId = dbWritable.update(Constants.ITEMS_TABLE,
                getUpdateObjectItems(subCategoryModel),
                "itemID = ?", new String[]{subCategoryModel.getProductId() + ""});
        if (updateId != -1) {
            return true;
        } else {
            return false;
        }

    }

    private ContentValues getUpdateObjectItems(SubCategoryModel subCategoryModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.IS_DISPLAY, subCategoryModel.getIsDisplay());
        return contentValues;
    }


    public boolean deleteItems(String itemId) {
        return dbWritable.delete(Constants.ITEMS_TABLE, "itemID = ? ", new String[]{itemId+""}) > 0;

    }


    public ArrayList<SubCategoryModel> getAllItems(String categoryId) {
        ArrayList<SubCategoryModel> subCategoryModelArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery("SELECT * FROM " + Constants.ITEMS_TABLE + " WHERE "
                + Constants.CAT_ID_ITEM + " = '" + Integer.parseInt(categoryId) + "' COLLATE NOCASE ", null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    SubCategoryModel subCategoryModel = new SubCategoryModel();
                    subCategoryModel.setCategoryId(cursor.getInt(cursor.getColumnIndex(Constants.CAT_ID_ITEM)));
                    subCategoryModel.setProductId(cursor.getInt(cursor.getColumnIndex(Constants.ITEM_ID)));
                    subCategoryModel.setProductName(cursor.getString(cursor.getColumnIndex(Constants.ITEM_NAME)));
                    subCategoryModel.setIsDisplay(cursor.getInt(cursor.getColumnIndex(Constants.IS_DISPLAY)));
                    subCategoryModelArrayList.add(subCategoryModel);

                } while (cursor.moveToNext());
            }
            return subCategoryModelArrayList;
        }
        return subCategoryModelArrayList;
    }

    /*items finish*/


}
