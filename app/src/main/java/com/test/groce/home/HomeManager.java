package com.test.groce.home;

import android.content.Context;

import com.test.groce.databasehelper.DBHelperSingleton;
import com.test.groce.home.model.HomeModel;

import java.util.ArrayList;

public class HomeManager {
    private Context context;
    private HomeCallbacklistner.HomeManagerCallback homeManagerCallback;

    public HomeManager(Context context, HomeCallbacklistner.HomeManagerCallback homeManagerCallback) {
        this.context = context;
        this.homeManagerCallback = homeManagerCallback;
    }

    public void callGetHomeListFromLocal(){
        ArrayList<HomeModel>homeModelArrayList = new ArrayList<>();
        homeModelArrayList.addAll(DBHelperSingleton.getInstance().mDBDbHelper.getHomeArrayListFromLocal());

        if(homeModelArrayList.isEmpty()){
            homeManagerCallback.onError("No Data Found!");
        }else {
            homeManagerCallback.onSuccessHomeList(homeModelArrayList);
        }
    }
}
