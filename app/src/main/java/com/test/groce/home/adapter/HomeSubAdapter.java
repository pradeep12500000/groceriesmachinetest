package com.test.groce.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.test.groce.R;
import com.test.groce.home.model.HomeSubmodel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeSubAdapter extends RecyclerView.Adapter<HomeSubAdapter.ViewHolder> {
    private Context context;
    private ArrayList<HomeSubmodel> homeSubmodels;

    public HomeSubAdapter(Context context, ArrayList<HomeSubmodel> homeSubmodels) {
        this.context = context;
        this.homeSubmodels = homeSubmodels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_product_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewProductItem.setText(homeSubmodels.get(position).getProductName());
    }

    @Override
    public int getItemCount() {
        return homeSubmodels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewProductItem)
        TextView textViewProductItem;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
