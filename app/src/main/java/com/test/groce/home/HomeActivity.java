package com.test.groce.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.test.groce.R;
import com.test.groce.basic.BaseActivity;
import com.test.groce.category.CategoryActivity;
import com.test.groce.home.adapter.HomeAdapter;
import com.test.groce.home.model.HomeModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity implements HomeAdapter.HomeCallbackListner, HomeCallbacklistner.HomeView {

    @BindView(R.id.imageViewCategory)
    ImageView imageViewCategory;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.recylerViewGroceriesList)
    RecyclerView recylerViewGroceriesList;
    @BindView(R.id.textViewHeaderName)
    TextView textViewHeaderName;
    @BindView(R.id.imageViewNoRecord)
    ImageView imageViewNoRecord;
    @BindView(R.id.relativeLayoutNoRecordFound)
    RelativeLayout relativeLayoutNoRecordFound;
    private Context context = this;
    private ArrayList<HomeModel> homeModels;
    private HomePresenter homePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        homePresenter = new HomePresenter(context, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        homePresenter.callGetHomeListFromLocal();
    }

    private void setDataAdapter() {
        HomeAdapter subCategoryAdapter = new HomeAdapter(context, homeModels, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recylerViewGroceriesList) {
            recylerViewGroceriesList.setAdapter(subCategoryAdapter);
            recylerViewGroceriesList.setLayoutManager(layoutManager);
        }
    }


    @OnClick(R.id.imageViewCategory)
    public void onViewClicked() {
        startActivity(new Intent(context, CategoryActivity.class));
    }

    @Override
    public void HomeCategoryItemClick(int position) {

    }

    @Override
    public void onSuccessHomeList(ArrayList<HomeModel> homeModelArrayList) {
        homeModels = new ArrayList<>();
        homeModels.addAll(homeModelArrayList);
        setDataAdapter();
        relativeLayoutNoRecordFound.setVisibility(View.GONE);
        recylerViewGroceriesList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
        if(errorMessage.equalsIgnoreCase("No Data Found!")){
            relativeLayoutNoRecordFound.setVisibility(View.VISIBLE);
            recylerViewGroceriesList.setVisibility(View.GONE);
        }
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }
}
