package com.test.groce.home;

import com.test.groce.basic.BaseInterface;
import com.test.groce.home.model.HomeModel;

import java.util.ArrayList;

public class HomeCallbacklistner {
    public interface HomeView extends BaseInterface {
        void onSuccessHomeList(ArrayList<HomeModel> homeModelArrayList);
    }

    public interface HomeManagerCallback{
        void onSuccessHomeList(ArrayList<HomeModel> homeModelArrayList);
        void onError(String errorMessage);
    }

}
