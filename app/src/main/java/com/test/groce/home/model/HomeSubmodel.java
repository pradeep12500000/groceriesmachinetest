package com.test.groce.home.model;

public class HomeSubmodel {
    int ProductId ;
    int categoryId;
    int isDisplay =0;
    String ProductName;

    public HomeSubmodel() {
    }

    public HomeSubmodel(int productId, int categoryId, int isDisplay,String productName) {
        ProductId = productId;
        this.categoryId = categoryId;
        this.isDisplay = isDisplay;
        ProductName = productName;
    }

    public int getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(int isDisplay) {
        this.isDisplay = isDisplay;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }
}
