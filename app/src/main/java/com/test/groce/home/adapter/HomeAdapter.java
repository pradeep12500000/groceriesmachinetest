package com.test.groce.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.test.groce.R;
import com.test.groce.home.model.HomeModel;
import com.test.groce.home.model.HomeSubmodel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    private Context context;
    private ArrayList<HomeModel> homeModels;
    private ArrayList<HomeSubmodel> homeSubmodelArrayList;
    private HomeCallbackListner homeCallbackListner;

    public HomeAdapter(Context context, ArrayList<HomeModel> homeModels, HomeCallbackListner homeCallbackListner) {
        this.context = context;
        this.homeModels = homeModels;
        this.homeCallbackListner = homeCallbackListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_home_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCategoryName.setText(homeModels.get(position).getCategoryName());

        homeSubmodelArrayList = new ArrayList<>();
        homeSubmodelArrayList.addAll(homeModels.get(position).getHomeSubmodel());
        HomeSubAdapter homeSubAdapter = new HomeSubAdapter(context, homeSubmodelArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewProduct) {
            holder.recyclerViewProduct.setLayoutManager(layoutManager);
            holder.recyclerViewProduct.setAdapter(homeSubAdapter);
        }

    }

    @Override
    public int getItemCount() {
        return homeModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCategoryName)
        TextView textViewCategoryName;
        @BindView(R.id.recyclerViewProduct)
        RecyclerView recyclerViewProduct;
        @BindView(R.id.cardClick)
        CardView cardClick;

        @OnClick(R.id.cardClick)
        public void onViewClicked() {
            homeCallbackListner.HomeCategoryItemClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface HomeCallbackListner {
        void HomeCategoryItemClick(int position);
    }
}
