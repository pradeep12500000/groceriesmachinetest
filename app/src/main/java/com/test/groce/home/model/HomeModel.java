package com.test.groce.home.model;

import java.util.ArrayList;

public class HomeModel {
    int CategoryId;
    String CategoryName;
    ArrayList<HomeSubmodel> homeSubmodel;

    public HomeModel() {
    }

    public HomeModel(int categoryId, String categoryName, ArrayList<HomeSubmodel> homeSubmodel) {
        CategoryId = categoryId;
        CategoryName = categoryName;
        this.homeSubmodel = homeSubmodel;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public ArrayList<HomeSubmodel> getHomeSubmodel() {
        return homeSubmodel;
    }

    public void setHomeSubmodel(ArrayList<HomeSubmodel> homeSubmodel) {
        this.homeSubmodel = homeSubmodel;
    }
}
