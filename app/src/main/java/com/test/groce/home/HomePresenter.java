package com.test.groce.home;

import android.content.Context;

import com.test.groce.home.model.HomeModel;

import java.util.ArrayList;

public class HomePresenter implements HomeCallbacklistner.HomeManagerCallback {
    private Context context;
    private HomeCallbacklistner.HomeView homeView;
    private HomeManager homeManager;

    public HomePresenter(Context context, HomeCallbacklistner.HomeView homeView) {
        this.context = context;
        this.homeView = homeView;
        this.homeManager = new HomeManager(context,this);
    }

    public void callGetHomeListFromLocal(){
        homeManager.callGetHomeListFromLocal();
    }

    @Override
    public void onSuccessHomeList(ArrayList<HomeModel> homeModelArrayList) {
        homeView.onSuccessHomeList(homeModelArrayList);
    }

    @Override
    public void onError(String errorMessage) {
       homeView.onError(errorMessage);
    }
}
