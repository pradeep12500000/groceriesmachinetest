package com.test.groce.basic;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;


public class BaseActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    public  String ShareMessage_REFERAL = "" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(BaseActivity.this);
    }



    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void hideLoader() {
        try {
            if(null != progressDialog)
                progressDialog.dismiss();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showLoader() {
        try{
            progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public void showLog(String tag, String msg) {
        Log.v(tag,msg);
    }

    public boolean isInternetConneted() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting() && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }
}
