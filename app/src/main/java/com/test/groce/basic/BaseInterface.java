package com.test.groce.basic;


public interface BaseInterface {
    void onError(String errorMessage);
    void onShowLoader();
    void onHideLoader();

}
